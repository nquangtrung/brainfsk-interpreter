#!/usr/bin/python

import sys

class BrainFsk:
	def __init__(self):
		self.reset()

	def run(self, source):
		source = self.cleanup(source)
		codeptr = 0
		while True:
			codeptr = self.onNextToken(codeptr, source)
			if codeptr >= len(source):
				break;

	def reset(self):
		self.memory = [0]
		self.pointer = 0
		self.open_bracket = []

	def mem_size(self):
		return len(self.memory)

	def mem_value(self):
		return self.memory[self.pointer]

	def inc(self):
		self.memory[self.pointer] = self.memory[self.pointer] + 1

	def dec(self):
		self.memory[self.pointer] = self.memory[self.pointer] - 1

	def next(self):
		self.pointer = self.pointer + 1
		if self.pointer >= self.mem_size():
			self.memory[self.pointer:] = [0]

	def prev(self):
		self.pointer = self.pointer - 1
		if self.pointer < 0:
			self.pointer = 0

	def is_zero(self):
		return self.mem_value() == 0

	def print_mem_value(self):
		sys.stdout.write(chr(self.mem_value()))

	def set_mem_value(self, value):
		self.memory[self.pointer] = value

	def find_matching_bracket(self, codeptr, source):
		brackets = 0
		for i, token in enumerate(source[codeptr:]):
			if token == '[':
				brackets = brackets + 1
			elif token == ']':
				brackets = brackets - 1
			if brackets == 0:
				return i + codeptr

		return -1

	def read_into_stdin(self):
		return sys.stdin.read(1)

	def onNextToken(self, codeptr, source):
		token = source[codeptr]

		# self.print_memory()
		# self.print_bracket()
		# print 'pointer: %d %s'% (self.pointer, token)

		if token == '+':
			self.inc()
		elif token == '-':
			self.dec()
		elif token == '>':
			self.next()
		elif token == '<':
			self.prev()
		elif token == '.':
			self.print_mem_value()
		elif token == ',':
			# Buggy, should consider implement this some other way
			# This will take input from stdin
			# self.set_mem_value(self.read_into_stdin())
			pass
		elif token == '[':
			if not self.is_zero():
				self.open_bracket.append(codeptr)
				return codeptr + 1
			else:
				return self.find_matching_bracket(codeptr, source) + 1
		elif token == ']':
			old_codeptr = self.open_bracket.pop()
			if self.is_zero():
				return codeptr + 1
			else:
				return old_codeptr

		return codeptr + 1

	def cleanup(self, source):
		return filter(lambda x: x in ['.', ',', '[', ']', '<', '>', '+', '-'], source)

	def print_memory(self):
		self.print_list(self.memory)

	def print_bracket(self):
		self.print_list(self.open_bracket)

	def print_list(self, the_list):
		print ', '.join([str(item) for item in the_list])

def main():
	if len(sys.argv) >= 2:
		f = open(sys.argv[1], "r")
		source = f.read()
		f.close()

		interpreter = BrainFsk()
		interpreter.run(source)
		if len(sys.argv) > 2 and sys.argv[2] == "debug":
			interpreter.print_memory()
	else: print "Usage:", sys.argv[0], "filename [debug]"

if __name__ == "__main__": main()